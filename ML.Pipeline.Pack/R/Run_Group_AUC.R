#' Run_Group_AUC
#'
#' @export

Run_Group_AUC<-function(strategy=NULL,
                        method=NULL,
                        Freq_or_Acc=NULL,
                        ncores=6){

### creates matrix with best cutoff

get.AUC<-function(myL){
  data<-get(load(myL))
  name.fix<-sub('W0_W4','W0&W4',myL)
  name.split<-strsplit(name.fix,'_')[[1]]
  Time=name.split[grep('W',name.split)]
  drug=name.split[2]
  cutoff=as.numeric(name.split[5])
  name.split2<-strsplit(name.split[1],'/')[[1]]
  Kit=name.split2[2]
  pred.list<-mclapply(data, function(L){cbind.data.frame(obs=factor(L$data.test[,'ytest'],levels=c(0,1)),
                                                         pred=L$pred.test[,method])},mc.cores=ncores)
  pred.mat<-do.call('rbind.data.frame', pred.list)
  rocobj<-roc(pred.mat$obs,pred.mat$pred)
  AUC.mat<-cbind.data.frame(AUC=as.numeric(auc(rocobj)),
                            confmin=ci.auc(rocobj)[1],
                            confmax=ci.auc(rocobj)[3],
                            Time=Time,
                            drug=drug,
                            cutoff=cutoff,
                            Kit=Kit)
  return(AUC.mat)
}


  bag.cutname<-paste0('_bag','_',strategy,Freq_or_Acc)

  train.cutname<-paste0('train','_',strategy)

## Bagging Results

fname.INF<-paste0('results/INF/outputs/')
INF.files<-list.files(path=fname.INF, pattern=bag.cutname)
INF.files.l<-lapply(INF.files,function(m){paste0(fname.INF,m)})
INF.files<-unlist(INF.files.l)

fname.CVD<-paste0('results/CVD.INF/outputs/')
CVD.files<-list.files(path=fname.CVD, pattern=bag.cutname)
CVD.files.l<-lapply(CVD.files,function(m){paste0(fname.CVD,m)})
CVD.files<-unlist(CVD.files.l)

all.files<-c(INF.files,CVD.files)

all.files.l<-lapply(all.files,function(m){m})

all.AUC<-do.call('rbind.data.frame', mclapply(all.files.l, get.AUC, mc.cores=ncores))

save(all.AUC, file=paste0('results/INF/tables/all.AUC_bag_',strategy,'_',method,Freq_or_Acc,'.Rda'))

## Training Data ##

fname.INF<-paste0('results/INF/outputs/')
INF.files<-list.files(path=fname.INF, pattern=train.cutname)
INF.files.l<-lapply(INF.files,function(m){paste0(fname.INF,m)})
INF.files<-unlist(INF.files.l)

fname.CVD<-paste0('results/CVD.INF/outputs/')
CVD.files<-list.files(path=fname.CVD, pattern=train.cutname)
CVD.files.l<-lapply(CVD.files,function(m){paste0(fname.CVD,m)})
CVD.files<-unlist(CVD.files.l)

all.files<-c(INF.files,CVD.files)

all.files.l<-lapply(all.files,function(m){m})

all.AUC<-do.call('rbind.data.frame', mclapply(all.files.l, get.AUC, mc.cores=ncores))

save(all.AUC, file=paste0('results/INF/tables/all.AUC_train_',strategy,'_',method,'.Rda'))

}



