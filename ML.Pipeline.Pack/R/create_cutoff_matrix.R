#' create_cutoff_matrix
#'
#' @export

create_cutoff_matrix<-function(strategy=NULL,
                               ncores=NULL){

  cutname<-paste0('bag','_',strategy)

  get.AUC<-function(myL){
    data<-get(load(myL))
    name.fix1<-sub('W0_W4','W0&W4',myL)
    name.fix2<-sub('rapFreq','rap_Freq',name.fix1)
    name.fix3<-sub('rapTrainACC','rap_TrainAcc',name.fix2)
    name.fix<-sub('rapAcc','rap_Acc',name.fix3)
    name.split<-strsplit(name.fix,'_')[[1]]
    Time=name.split[3]
    drug=name.split[2]
    cutoff=as.numeric(name.split[5])
    #TEST<-name.split[10]
    name.split2<-strsplit(name.split[1],'/')[[1]]
    Kit=name.split2[2]
    name.split3<-strsplit(name.split[9],'[.]')[[1]]
    FreqAcc<-name.split3[1]
    AUC.mat<-NULL
    AUC.list<-mclapply(c('pam','glm','pls','ensemble','ensemble.log'), function(Method){
    pred.list<-lapply(data, function(L){cbind.data.frame(obs=factor(L$data.test[,'ytest'],levels=c(0,1)),
                                                           pred=L$pred.test[,Method])})
    pred.mat<-do.call('rbind.data.frame', pred.list)
    rocobj<-roc(pred.mat$obs,pred.mat$pred)

    AUC.mat.tmp<-cbind.data.frame(AUC=as.numeric(auc(rocobj)),
                              confmin=ci.auc(rocobj)[1],
                              confmax=ci.auc(rocobj)[3],
                              Time=Time,
                              drug=drug,
                              cutoff=cutoff,
                              Kit=Kit,
                              FreqAcc=FreqAcc,
                              Method=Method)

    },mc.cores=ncores)
    AUC.mat<-do.call('rbind.data.frame', AUC.list)
    return(AUC.mat)
  }

  stack.best<-function(data, DRUG, TIME, KIT, FREQACC, METHOD){
    data.sub<-subset(data, data$drug==DRUG & data$Time==TIME & data$Kit==KIT)
    best<-data.sub[which(data.sub$AUC==max(data.sub$AUC)),]
    return(best)
  }

  fname.INF<-paste0('results/INF/outputs/')
  INF.files<-list.files(path=fname.INF, pattern=cutname)
  INF.files.l<-lapply(INF.files,function(m){paste0(fname.INF,m)})
  INF.files<-unlist(INF.files.l)

  fname.CVD<-paste0('results/CVD.INF/outputs/')
  CVD.files<-list.files(path=fname.CVD, pattern=cutname)
  CVD.files.l<-lapply(CVD.files,function(m){paste0(fname.CVD,m)})
  CVD.files<-unlist(CVD.files.l)

  all.files<-c(INF.files,CVD.files)

  all.files<-all.files[-grep('NNetTEST',all.files)]

  all.files.l<-lapply(all.files,function(m){m})

  all.AUC<-do.call('rbind.data.frame', mclapply(all.files.l, get.AUC, mc.cores=ncores))

  save(all.AUC,file=paste0('results/INF/tables/',strategy,'_','cutoff.Rda'))

  conditions<-subset(all.AUC, all.AUC$cutoff==0.70 & all.AUC$Method=='glm' & all.AUC$FreqAcc=='Freq', select=c('Time','drug','Kit'))
  conditions.l<-as.list(as.data.frame(t(conditions),stringsAsFactors=FALSE))

  #data<-all.AUC
  data<-subset(all.AUC, FreqAcc=='Freq')

  max.AUC<-mclapply(conditions.l, function(L, data=data){stack.best(data=data, TIME=L[1],DRUG=L[2],KIT=L[3])},data=data, mc.cores=ncores)

  max.AUC.m<-do.call('rbind.data.frame', max.AUC)
  save(max.AUC.m,file=paste0('results/INF/tables/',strategy,'_','maxAUC.Rda'))

  return(max.AUC.m)
}
