#' Plot_Group_Accu_Opti_Shuff
#'
#' @export

Plot_Group_Accu_Opti_Shuff<-function(train=NULL,
                                     method=NULL,
                                     strategy=NULL,
                                     ncores=6){


# if(train==TRUE){
#   fname<-paste0('results/INF/tables/Best.AUC_train_',strategy,'_',method,'.Rda')
# }else if(train==FALSE){
  fname<-paste0('results/INF/tables/',strategy,'_maxAUC.Rda')
#}

Best.AUC<-get(load(fname))

conditions<-subset(Best.AUC, select=c('Time','drug','Kit','cutoff','FreqAcc','Method'))
conditions.l<-as.list(as.data.frame(t(conditions),stringsAsFactors=FALSE))

stack.best<-function(data, DRUG, TIME, KIT, CUT, FREQACC, METHOD){
best<-subset(data, data$drug==DRUG & data$Time==TIME & data$Kit==KIT & data$cutoff==as.numeric(CUT))
return(best)
}

# if(train==TRUE){
#   fname<-paste0('results/INF/tables/best.ACC_train_',strategy,'_',method,'.Rda')
# }else if(train==FALSE){
  #fname<-paste0('results/INF/tables/best.ACC_bag_',strategy,'_',method,'.Rda')
#}

# best.ACC<-get(load(fname))
#
# if(train==TRUE){
#   fname<-paste0('results/shuffle/INF/tables/best.ACC_train_',strategy,'_',method,'.Rda')
# }else if(train==FALSE){
  fname<-paste0('results/shuffle/INF/tables/All.shuffle.bag.',strategy,'.Rda')
#}

All.Shuff.AUC<-get(load(fname))
Best.Shuff.AUC<-do.call('rbind.data.frame', mclapply(conditions.l, function(L, data=data){stack.best(data=data,TIME=L[1],DRUG=L[2],KIT=L[3],CUT=L[4])},data=All.Shuff.ACC, mc.cores=ncores))

name.fix1<-gsub('Etan','Etanercept',Best.Shuff.ACC$drug)
name.fix2<-gsub('Tofa','Tofacitinib',name.fix1)
Best.Shuff.ACC$drug<-name.fix2

save(Best.Shuff.ACC, file=paste0('results/shuffle/INF/tables/Best.Shuff.ACC_bag_',strategy,'_',method,'.Rda'))

Best.ACC<-subset(Best.ACC,select=-c(q025, q075))
Best.ACC$Shuff_or_Reg='Regular'

Best.Shuff.ACC<-subset(Best.Shuff.ACC,select=-c(Sensitivity, Specificity))

data.plot<-rbind(Best.Shuff.ACC,Best.ACC )

data.plot<-subset(data.plot,Kit=='INF')

#D.ACC$drug<-mapvalues(D.ACC$drug,from=c('Etan','Tofa'),to=c('Etanercept','Tofacitinib'))
data.plot$drug<-factor(data.plot$drug,levels=c('Tofacitinib','Etanercept'))
data.plot$Time<-factor(data.plot$Time,levels=c('W0','W4','W0&W4'))


gf<-ggplot(data.plot,aes(x=Time,y=mean.ACC,fill=Shuff_or_Reg))+
           facet_grid(.~drug)+
           scale_fill_manual(values=c('lightblue','blue'))+
           #scale_y_continuous(breaks=seq(0.5,1,0.1),limits=c(0.5,1), oob = 'censor')+
           geom_bar(stat='identity',position=position_dodge(width=0.75))+
           geom_errorbar(aes(ymin=mean.ACC-2*sem,ymax=mean.ACC+2*sem),position=position_dodge(width=0.75),width=0.25)+
           #geom_text(data=D.summ[D.summ$kit=='INF',], aes(label=paste0(round(100*mean.Ac,2)),y = mean.Ac + 0.05), nudge_x = -0.15)+
           #geom_text(data=D.summ[D.summ$kit=='CVD.INF',], aes(label=paste0(round(100*mean.Ac,2)),y = mean.Ac + 0.05, size=1), nudge_x = 0.15)+
           theme_bw()+
           labs(y='Mean Accuracy')

out.name<-paste(strategy,method,sep='.')

if(train==TRUE){
 ggsave(plot = gf, filename = paste0('results/shuffle/INF/figs/Best.ACC.train_',out.name,'.pdf'),width=8,height=4)
}else if(train==FALSE){
ggsave(plot = gf, filename = paste0('results/shuffle/INF/figs/Best.ACC.bag_',out.name,'.pdf'),width=8,height=4)
}
}
