#' Stack_AUC_by_drug
#'
#' @export

Stack_AUC_by_drug<-function(Biomarker.Set=NULL,
                              method=NULL,
                              strategy=NULL,
                              drug=NULL,
                              time=NULL,
                              ncores=6){
  get.AUC<-function(myL){
    data<-get(load(myL))
    name.fix<-sub('W0_W4','W0&W4',myL)
    name.split<-strsplit(name.fix,'_')[[1]]
    Time=name.split[grep('W',name.split)]
    drug=name.split[2]
    cutoff=as.numeric(name.split[5])
    pred.list<-mclapply(data, function(L){cbind.data.frame(obs=factor(L$data.test[,'ytest'],levels=c(0,1)), pred=L$pred.test[,method])},mc.cores=ncores)
    pred.mat<-do.call('rbind.data.frame', pred.list)
    rocobj<-roc(pred.mat$obs,pred.mat$pred)
    AUC.mat<-cbind.data.frame(AUC=as.numeric(auc(rocobj)),confmin=ci.auc(rocobj)[1],confmax=ci.auc(rocobj)[3],Time=Time,drug=drug,cutoff=cutoff)
    return(AUC.mat)
  }

  fname<-paste0('results/',Biomarker.Set,'/outputs/')

  #cutname<-paste0(bagging.cutoff,'_bag')

  all.files<-list.files(path=fname, pattern='_bag')
  all.files.drug<-all.files[grep(drug, all.files)]
  all.files.time<-all.files.drug[grep(time, all.files.drug)]
  if(time!='W0_W4'){
    all.files.time<-all.files.time[-grep('W0_W4', all.files.time)]
  }
  all.files.strat<-all.files.time[grep(strategy, all.files.time)]

  all.files.l<-lapply(all.files.strat,function(m){paste0(fname,m)})

  all.AUC<-do.call('rbind.data.frame', mclapply(all.files.l, get.AUC, mc.cores=ncores))
}
