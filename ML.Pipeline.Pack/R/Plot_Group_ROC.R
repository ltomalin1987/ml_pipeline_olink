#' Plot_Group_ROC
#'
#' @export

Plot_Group_ROC<-function(bagging.cutoff=NULL,
                         cols=c('red','purple','blue'),
                         train=NULL,
                         strategy=NULL,
                         group=NULL,
                         method=NULL,
                         ncores=6){

if(train==TRUE){
  fname<-paste0('results/INF/tables/all.Pred_train_',strategy,'.Rda')
}else if(train==FALSE){
  fname<-paste0('results/INF/tables/all.Pred_bag_',strategy,'.Rda')
}

  output=paste0(bagging.cutoff,'_',method,'_')

  ### Stack Predictions and plot ROC curves ###

  All.Preds<-get(load(fname))

  Preds.cut<-subset(All.Preds,cutoff==bagging.cutoff)

  Preds.INF<-subset(Preds.cut, Kit=='INF')
  Preds.INF.Tofa<-subset(Preds.INF, drug=='Tofa')
  Preds.INF.Etan<-subset(Preds.INF, drug=='Etan')

  Method<-paste0('pred.test.',method)

  Preds.plot.Tofa<-data.frame(obs=Preds.INF.Tofa[,'y'], pred=Preds.INF.Tofa[,Method], group=Preds.INF.Tofa[,group])

  if(train==TRUE){tmod<-'_train'}else {tmod=''}

  plotROCbyGroups(data=Preds.plot.Tofa,
                  cols=cols,
                  output=paste0('results/INF/figs/',output,'Tofa_',strategy,'_',tmod),
                  wd=4,
                  ht=4,
                  graftitle='')

  Preds.plot.Etan<-data.frame(obs=Preds.INF.Etan[,'y'], pred=Preds.INF.Etan[,Method], group=Preds.INF.Etan[,group])

  plotROCbyGroups(data=Preds.plot.Etan,
                  cols=cols,
                  output=paste0('results/INF/figs/',output,'Etan_',strategy,'_',tmod),
                  wd=4,
                  ht=4,
                  graftitle='')

  Preds.CVD<-subset(All.Preds,Kit='CVD.INF')
  Preds.CVD.Tofa<-subset(Preds.CVD, drug=='Tofa')
  Preds.CVD.Etan<-subset(Preds.CVD, drug=='Etan')

  Preds.plot.Tofa<-data.frame(obs=Preds.CVD.Tofa[,'y'], pred=Preds.CVD.Tofa[,Method], group=Preds.CVD.Tofa[,group])

  plotROCbyGroups(data=Preds.plot.Tofa,
                  cols=cols,
                  output=paste0('results/INF/figs/',output,'Tofa_',strategy,'_',tmod),
                  wd=4,
                  ht=4,
                  graftitle='')

  Preds.plot.Etan<-data.frame(obs=Preds.CVD.Etan[,'y'], pred=Preds.CVD.Etan[,Method], group=Preds.CVD.Etan[,group])

  plotROCbyGroups(data=Preds.plot.Etan,
                  cols=cols,
                  output=paste0('results/INF/figs/',output,'Etan_',strategy,'_',tmod),
                  wd=4,
                  ht=4,
                  graftitle='')

}
